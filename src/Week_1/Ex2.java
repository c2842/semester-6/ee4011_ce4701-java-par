package Week_1;

public class Ex2 {
    public static void main(String[] args) {
        Ex2_Method(1, 2);
        Ex2_Method(2, 3);
    }

    static void Ex2_Method(int a, int b){
        int largest = a;
        if(b> largest){
            largest = b;
        }
        String isEven = "Odd";
        if((largest % 2) == 0){
            isEven = "Even";
        }

        System.out.printf("%d is the Largest number and it is %s %n", largest, isEven);
    }
}
