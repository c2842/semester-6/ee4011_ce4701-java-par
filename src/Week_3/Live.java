package Week_3;

public class Live {

    public static void main(String[] args) {
        Integer[] array = {1,2,3,4,6,2,3,2};


        System.out.println(is_present(array, 5));
        System.out.println(is_present(array, 2));
        System.out.println(is_present(array, 3));
        System.out.println(is_present(array, 6));
        System.out.println(is_present_generic(array, 6));

        System.out.println(count(array, 5));
        System.out.println(count(array, 2));
        System.out.println(count(array, 3));
    }


    public static boolean is_present(Integer[] array, Integer search_value){
        for(int value: array){
            if(value == search_value){
                return true;
            }
        }
        return false;
    }

    public static <T> boolean is_present_generic(T[] array, T search_value){
        for(T value: array){
            if(value == search_value){
                return true;
            }
        }
        return false;
    }


    public static int count(Integer[] array, Integer search_value){
        int result = 0;

        for(int value: array){
            if(value == search_value){
                result += 1;
            }
        }

        return result;
    }
}
