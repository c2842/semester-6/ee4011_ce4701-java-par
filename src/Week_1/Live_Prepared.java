package Week_1;

public class Live_Prepared {
    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            double rand = Math.random() * i;
            System.out.printf("%.3f meters is %.3f feet %n", rand, meter_to_feet(rand));
        }
        System.out.println();
        for (int i = 0; i < 10; i++) {
            double rand = Math.random() * i;
            System.out.printf("%.3f feet is %.3f meters %n", rand, feet_to_meter(rand));
        }
    }

    static double meter_to_feet(double meter) {
        return meter * 3.28084;
    }

    static double feet_to_meter(double feet) {
        return feet * 0.3048;
    }
}
