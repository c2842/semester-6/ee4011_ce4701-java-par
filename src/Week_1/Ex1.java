package Week_1;

public class Ex1 {
    public static void main(String[] args) {
        FizzBuzz(23);

        System.out.println();

        FizzBuzzMemory(23);
    }

    /**
     * FizzBuzz
     * Using modulo it determines if the output should be fizz or buzz
     *
     * @param n Max digit to go up to
     */
    static void FizzBuzz(int n) {
        for (int i = 1; i < n; i++) {
            System.out.printf("%d ", i);
            if ((i % 3) == 0) {
                System.out.print("Fizz");
            }
            if ((i % 5) == 0) {
                System.out.print("Buzz");
            }
            System.out.printf("%n");
        }
    }

    /**
     * FizzBuzz
     * but a more memory efficient one that can deal with higher numbers easier.
     *
     * @param n Max digit to go up to
     */
    static void FizzBuzzMemory(int n) {
        // counters
        int counter_three = 0;
        int counter_five = 0;

        for (int i = 1; i < n; i++) {
            System.out.printf("%d ", i);

            counter_three += 1;
            counter_five += 1;


            if (counter_three == 3) {
                System.out.print("Fizz");
                counter_three = 0;
            }
            if (counter_five == 5) {
                System.out.print("Buzz");
                counter_five = 0;
            }
            System.out.printf("%n");
        }
    }
}
