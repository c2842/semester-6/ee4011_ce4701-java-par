package Week_1;

public class Ex3_BasicsRevision {
    public static void main(String[] args) {
        int x = 10;
        double y = 0.63;
        double z = x * y;
        System.out.printf("x=%d, y=%.2f, z=%.2f %n", x, y, z);
        double result = add(10.2+2.0*3.5, z*2.0);
        System.out.print("Result is ");
        System.out.println(result);
        System.out.printf("Result is %.2f %n",result);

        System.out.printf("Sum of 1 to 10 is %d %n", sum1st10());
        System.out.printf("Sum of 1 to 50 is %d %n", sum(50));
    }

    static double add(double a, double b){
        return a+b;
    }

    static int sum1st10(){
        int result = 0;
        for(int i=1; i<=10;i++){
            result += i;
        }
        return result;
    }

    static int sum(int n){
        int result = 0;
        for(int i=1; i<=n;i++){
            result += i;
        }
        return result;
    }

    static int sum1st10_2(){
        return sum(10);
    }
}
